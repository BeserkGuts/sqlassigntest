
--1. Show all information about the bond with the CUSIP '28717RH95'.
/* SELECT * FROM bond 
WHERE CUSIP ='28717RH95' */

--2. Show all information about all bonds, in order from shortest (earliest maturity) to longest (latest maturity).
/* SELECT * FROM bond 
ORDER BY maturity ASC */

-----/*SELECT (quantity * price) AS 'Value of Bond'
------FROM bond */

--3. Calculate the value of this bond portfolio, as the sum of the product of each bond's quantity and price.
/* SELECT SUM(quantity * price) AS 'Value of Bond'
FROM bond */

--4. Show the annual return for each bond, as the product of the quantity and the coupon. Note that the coupon rates are quoted as whole percentage points, so to use them here you will divide their values by 100.
/* SELECT CUSIP, quantity, price, coupon, price*((quantity * (coupon/100))) AS 'ANNUAL RETURN' 
FROM bond */


--5. Show bonds only of a certain quality and above, for example those bonds with ratings at least AA2. (Don't resort to regular-expression or other string-matching tricks; use the bond-rating ordinal.)
--the _ could be replaced with a %
/* SELECT * FROM bond
WHERE rating like 'AA_' AND rating NOT LIKE 'AA3'
ORDER BY rating */


--6. Show the average price and coupon rate for all bonds of each bond rating.
/* SELECT avg(price), avg(coupon), rating FROM bond
GROUP BY rating */

--7. Calculate the yield for each bond, as the ratio of coupon to price. Then, identify bonds that we might consider to be overpriced, as those whose yield is less than the expected yield given the rating of the bond.
 SELECT ID, CUSIP, quantity, maturity, bond.rating, ((coupon)/price) AS 'Calculated Yield', expected_yield, (expected_yield -(coupon/price)) AS 'Expected - Calculated Yield' FROM bond
JOIN rating ON bond.rating = rating.rating 
WHERE (expected_yield -(coupon/price)) >0


