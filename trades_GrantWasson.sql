
--Find all trades by a given trader on a given stock - for example the trader with ID=1 and the ticker 'MRK'. This involves joining from trader to position and then to trades twice, through the opening- and closing-trade FKs.
  SELECT trader.first_name, trader.last_name, trade.stock, trade.ID AS 'Trade ID', position.opening_trade_ID, position.closing_trade_ID
FROM trader 
JOIN position
ON trader.id = position.trader_ID
JOIN trade
--ON position.opening_trade_ID = trade.ID
ON position.closing_trade_ID = trade.ID OR position.opening_trade_ID = trade.ID
where stock='AAPL' AND trader.ID='1'
AND position.closing_trade_ID ! = position.opening_trade_ID
ORDER BY trader.ID



--Find the total profit or loss for a given trader over the day, as the sum of the product of trade size and price for all sales, minus the sum of the product of size and price for all buys.
SELECT SUM(CASE WHEN buy =1 then trade.price*trade.size END)-SUM(CASE when buy=0 then trade.price*trade.size END) AS 'profit/loss for the day'
FROM trader 
JOIN position
ON trader.id = position.trader_ID
JOIN trade
ON position.closing_trade_ID = trade.ID OR position.closing_trade_ID =trade.ID
where trader.ID=1 AND position.closing_trade_ID is NOT NULL 
AND position.closing_trade_ID ! = position.opening_trade_ID

--Develop a view that shows profit or loss for all traders.
SELECT trader.ID, SUM(CASE WHEN buy =1 then trade.price*trade.size END)-SUM(CASE when buy=0 then trade.price*trade.size END) AS 'profit/loss for the day'
FROM trader 
JOIN position
ON trader.id = position.trader_ID
JOIN trade
ON position.closing_trade_ID = trade.ID OR position.closing_trade_ID =trade.ID
WHERE position.closing_trade_ID is NOT NULL
AND position.closing_trade_ID ! = position.opening_trade_ID
GROUP BY trader.ID